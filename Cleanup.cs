﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ABC
{
    public static class Cleanup
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Cleanup(string sourceDirectory)
        {
            Log.Debug(string.Format("Enter logCleanup Directory: [{0}]", sourceDirectory));

            try
            {
                //Listing all the log file
                List<String> files = Directory.GetFiles(sourceDirectory).Select(Path.GetFileName).ToList();
                string lagdays = File.ReadAllText(PathLocation.TimelagFileDirectory);

                var fileName = from String s in files where s.Contains(".txt") && !s.Contains("services.txt") select s; //Retriving log files containing .txt in the name & except FolderWatcherService log file

                foreach (var file in fileName)
                {
                    try
                    {
                        string date = Regex.Match(file, @"\d{8}").Value; //Spliting the date string from the log file name

                        if (date != "")
                        {
                            DateTime dt = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture); //Converting date string to DateTime
                            TimeSpan ageOfFile = DateTime.Now - dt; //Calculating age of file from current date

                            if (ageOfFile > TimeSpan.FromDays(Convert.ToInt32(lagdays))) //If older then define time frame delete that file
                            {
                                File.Delete(sourceDirectory + file);
                                Log.Debug(string.Format("Deleted file: [{0}]", file));
                            }
                            else
                            {
                                //Log files which are not older then define time frame
                                Log.Debug(string.Format("Remaining file: [{0}]", file));
                            }
                        }
                        else
                        {
                            File.Delete(sourceDirectory + file);
                            Log.Debug(string.Format("Deleted file: [{0}]", file));
                        }
                    }
                    catch(Exception ex)
                    {
                        Log.Error(ex.Message);
                    }

                }
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
            }

            Log.Debug(string.Format("Exit logCleanup Directory: [{0}]", sourceDirectory));            
        }
    }
}
