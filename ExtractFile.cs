﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Ionic.Zip;
using System.IO;

namespace ABC
{
    public static class ExtractFile
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static int _retryCounter = 1;
               
        //Backing Up previous deployed file in ".zip" format
        //FileName format: servicesBackUp 2015_06_30_10_02_16 (example: servicesBackUp yyyy_MM_dd_HH_mm_ss.zip)
        public static void zipFile(string sourceDirectory, string outputDirectory)        
        {
            Log.Debug(string.Format("Enter zipFile() SourceDirectory: [{0}], OutputDirectory: [{1}]", sourceDirectory, outputDirectory));

            try
            {
                using (ZipFile zip = new ZipFile())
                {                    
                    zip.AddDirectory(sourceDirectory); //Make Zip file under root
                    zip.Save(outputDirectory + "servicesBackUp " + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".zip"); //FileName format: servicesBackUp 2015_06_30_10_02_16
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
            }

            Log.Debug(string.Format("Exit zipFile() SourceDirectory: [{0}], OutputDirectory: [{1}]", sourceDirectory, outputDirectory));
        }

        //Zip File extraction operation
        public static void extractFile(string sourceDirectory, string outputDirectory)
        {
            Log.Debug(string.Format("Enter extractFile() SourceDirectory: [{0}], OutputDirectory: [{1}]", sourceDirectory, outputDirectory));

            try
            {
                DirectoryInfo dir = new DirectoryInfo(sourceDirectory); //Source directory of the files to perform extract operation
                FileInfo[] Files = dir.GetFiles("*.zip");

                foreach (FileInfo file in Files)
                {
                    using (ZipFile zip = ZipFile.Read(sourceDirectory + file.Name))
                    {
                        foreach (ZipEntry e in zip)
                        {
                            e.Extract(outputDirectory, ExtractExistingFileAction.OverwriteSilently); //Extract to output directory and Overwrite if necessary
                        }
                    }

                    File.Delete(sourceDirectory + file.Name); //Delete the source ".zip" file from "Drop Directory" after extraxtion complete
                }  
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
            }

            Log.Debug(string.Format("Exit extractFile() SourceDirectory: [{0}], OutputDirectory: [{1}]", sourceDirectory, outputDirectory));
        }

        //Copy the new file to the Configured directory, where service is deployed
        public static void copyFile(string sourceDirectory, string outputDirectory)
        {
            Log.Debug(string.Format("Enter copyFile() SourceDirectory: [{0}], OutputDirectory: [{1}]", sourceDirectory, outputDirectory));            

            try
            {
                //Creating all the folders and subfolders
                foreach (string dirPath in Directory.GetDirectories(sourceDirectory, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(dirPath.Replace(sourceDirectory, outputDirectory));
                }

                //Copying file in all folders and their sub folder
                foreach (string newPath in Directory.GetFiles(sourceDirectory, ".", SearchOption.AllDirectories))
                {
                    File.Copy(newPath, newPath.Replace(sourceDirectory, outputDirectory), true);

                    Log.Debug(string.Format("Successfully completed copying file into: [{0}]", newPath));
                }

                Log.Debug(string.Format("Successfully completed copying all files."));

                //Delete operation on extracted files in Temporary directory after copy/ deploy complete
                DirectoryInfo dir = new DirectoryInfo(sourceDirectory);

                foreach(FileInfo files in dir.GetFiles())
                {
                    files.Delete();

                    Log.Debug(String.Format("File deleted from temporary directory. Filename: [{0}]", sourceDirectory + files));
                }

                foreach(DirectoryInfo directory in  dir.GetDirectories())
                {
                    directory.Delete(true);

                    Log.Debug(string.Format("Directory deleted from temporary directory. Directoryname: [{0}]", sourceDirectory + directory));
                }                
                
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
                Thread.Sleep(7000); //Wait 7sec before retry the file copy operation 

                if (_retryCounter < 11) //If failed retry 10 times
                {
                    //Keeping a log for retry and recording the RetryCounter
                    Log.Debug(string.Format("Retrying Copy operation. SourceDirectory: [{0}], OutputDirectory: [{1}], RetryCounter: [{2}]", sourceDirectory, outputDirectory, _retryCounter ));

                    _retryCounter++;
                    copyFile(sourceDirectory, outputDirectory);
                }
                else
                {
                    //Delete operation on extracted files in Temporary directory if copy/ deploy failed
                    DirectoryInfo dir = new DirectoryInfo(sourceDirectory);

                    foreach (FileInfo files in dir.GetFiles())
                    {
                        files.Delete();

                        Log.Debug(String.Format("File deleted from temporary directory. Filename: [{0}]", sourceDirectory + files));
                    }

                    foreach (DirectoryInfo directory in dir.GetDirectories())
                    {
                        directory.Delete(true);

                        Log.Debug(string.Format("Directory deleted from temporary directory. Directoryname: [{0}]", sourceDirectory + directory));
                    }

                    //Keep a log after if after 10 times service didn't get start and how many times retry performed
                    Log.Debug(string.Format("Failed to copy files. SourceDirectory: [{0}], OutputDirectory: [{1}], Totalretried: [{2}]", sourceDirectory, outputDirectory, (_retryCounter - 1)));
                }
            }

            Log.Debug(string.Format("Exit copyFile() SourceDirectory: [{0}], OutputDirectory: [{1}]", sourceDirectory, outputDirectory));
        }       

    }
}