﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Management;
using System.ServiceProcess;
using System.Threading;

namespace ABC
{
    public static class ServiceController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static System.ServiceProcess.ServiceController svc;
        private static int _retryCounter = 1;

        //Services stop operation
        public static void StopService(string serviceName)
        {
            Log.Debug(string.Format("Enter StopService() Servicename: [{0}]", serviceName));            

            try
            {
                svc = new System.ServiceProcess.ServiceController();

                svc.ServiceName = serviceName; //Receive service name from client
                string svcStatus = svc.Status.ToString(); //Retriving the service status                
                                
                if (svcStatus == "Running")
                {
                    svc.Stop();
                    svc.WaitForStatus(ServiceControllerStatus.Stopped); //Waiting for service to stop

                    //Keeping log after service stopped
                    Log.Debug(string.Format("Service stopped successfully. Servicename: [{0}]", serviceName));
                }
                else
                {
                    //Log if the service is already stopped
                    Log.Debug(string.Format("Service already stop. Servicename: [{0}]", serviceName));
                }                               
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Thread.Sleep(7000); //Wait 7sec before retry the stop operation          

                if(_retryCounter<11) //If failed retry 10 times
                {
                    //Keeping a log for retry and recording the RetryCounter
                    Log.Debug(string.Format("Retrying Stop operation. Servicename: [{0}] RetryCounter: [{1}]", serviceName, _retryCounter));

                    _retryCounter++;
                    StopService(serviceName); //Calling StopService() function to retry the stop the service
                }
                else
                {
                    //Keep a log after if after 10 times service didn't get start and how many times retry performed
                    Log.Debug(string.Format("Failed to stop service. Servicename: [{0}], Totalretried: [{1}]", serviceName, (_retryCounter - 1)));
                }               
            }            

            Log.Debug(string.Format("Exit StopService() Servicename: [{0}]", serviceName));

        }

       
        //Services start operation
        public static void StartService(string serviceName)
        {
            Log.Debug(string.Format("Enter StartService() Servicename: [{0}]", serviceName));            

            try
            {
                svc = new System.ServiceProcess.ServiceController();

                svc.ServiceName = serviceName; //Receive service name from client
                string svcStatus = svc.Status.ToString(); //Retriving the service status

                if (svcStatus == "Stopped")
                {
                    svc.Start();
                    svc.WaitForStatus(ServiceControllerStatus.Running); //Waiting for service to start  

                    Log.Debug(string.Format("Service started successfully. Servicename: [{0}]", serviceName));
                }
                else
                {
                    Log.Debug(string.Format("Service already running. Servicename: [{0}]", serviceName));
                }                               
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);                               
            }

            Log.Debug(string.Format("Exit StartService() Servicename: [{0}]", serviceName)); 

        }
        
        //Getting the services name which are installed and perform Stop/ Start operation
        public static void ListServices(string operation)
        {
            Log.Debug(string.Format("Enter ListServicesStatus()."));
            
            ArrayList serviceList = new ArrayList();                    

            //Retriving the services which are Log on as "emsc\" i.e. (emsc\username)
            System.Management.SelectQuery query = new System.Management.SelectQuery(string.Format(
                "select name from Win32_Service where startname LIKE'%emsc%'"));

            using (ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query))
            {
                foreach (ManagementObject service in searcher.Get())
                {
                    serviceList.Add((service["Name"]));  //Services name are adding in ArrayList                  
                }
            }

            if(operation == "Stop") //Perform Stop operation on the services
            {
                foreach (string serviceName in serviceList)
                {
                    if(serviceName !="WatcherService") //This service performing deploy operation, checking is geven so that it should not stop itself                 
                    {
                        StopService(serviceName); //Calling Stop function to stop the services
                    }                    
                }
            }
            else   //Perform Start operation on the services       
            {
                foreach (string serviceName in serviceList)
                {
                    if (serviceName != "WatcherService") //This service performing deploy operation, checking is geven because service is already running
                    {
                        StartService(serviceName); //Calling Start function to stop the services
                    }                  
                }
            }           

            Log.Debug(string.Format("Exit ListServicesStatus()"));            
        }
    }
}
