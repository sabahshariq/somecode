﻿//
// Shariq's ABC Test's
// 

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ABC.Server;
using ABC.DataModel;
using ABC.InternalInterfaces.ServiceProxies;

namespace ServicesUnitTests
{
    public partial class ABCServiceUnitTests
    {
        [TestMethod]
        public void IsAlive05()
        {
            var abcServer = new AbcServer();
            Assert.IsTrue(abcServer.IsAlive());
        }

        //Updated By Shariq -- Remote Server Test              
        private const string RemoteServer = "abc.com";

        [TestMethod]
        public void AbcServiceIsAlive051()
        {
            VerifyUtils.RemoteServer = RemoteServer;
            using (
                var adminServiceClient = new AbcServiceClient(VerifyUtils.AdminRemoteUrl,
                    VerifyUtils.AdminRemoteEndpointName))
            {
                Assert.IsTrue(adminServiceClient.IsAlive());
            }
        }

        //Updated By Shariq -- Remote Server Test              
        [TestMethod]
        public void StopService051()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();
            //var servicename = "ABCService";
            //var servicename = "DEFService";
            var servicename = "JKLService";

            VerifyUtils.RemoteServer = RemoteServer;
            using (
                var adminServiceClient = new AbcServiceClient(VerifyUtils.AdminRemoteUrl,
                    VerifyUtils.AdminRemoteEndpointName))
            {
                var result = adminServiceClient.StopService(machine, user, servicename);

                Assert.IsTrue(result);
            }
        }

        //Updated By Shariq -- Remote Server Test              
        [TestMethod]
        public void StartService051()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();
            //var servicename = "ABCService";
            //var servicename = "DEFService";
            var servicename = "JKLService";

            VerifyUtils.RemoteServer = RemoteServer;
            using (
                var adminServiceClient = new AbcServiceClient(VerifyUtils.AdminRemoteUrl,
                    VerifyUtils.AdminRemoteEndpointName))
            {
                var result = adminServiceClient.StartService(machine, user, servicename);

                Assert.IsTrue(result);
            }
        }

        //Updated By Shariq -- Remote Server Test
        [TestMethod]
        public void RestartService051()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();
            //var servicename = "ABCService";
            //var servicename = "DEFService";
            var servicename = "JKLService";

            VerifyUtils.RemoteServer = RemoteServer;
            using (
                var adminServiceClient = new AbcServiceClient(VerifyUtils.AdminRemoteUrl,
                    VerifyUtils.AdminRemoteEndpointName))
            {
                var result = adminServiceClient.RestartService(machine, user, servicename);

                Assert.IsTrue(result);
            }
        }

        //Updated By Shariq -- Remote Server Test              
        [TestMethod]
        public void ListServicesStatus051()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();

            VerifyUtils.RemoteServer = RemoteServer;
            using (
                var adminServiceClient = new AbcServiceClient(VerifyUtils.AdminRemoteUrl,
                    VerifyUtils.AdminRemoteEndpointName))
            {
                var results = adminServiceClient.ListServicesStatus(machine, user);

                Assert.IsTrue(results.Rows.Count > 0);
            }
        }

        //Updated By Shariq -- Local PC Test
        [TestMethod]
        public void StopService052()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();
            var servicename = "ABCService";
            //var servicename = "DEFService";
            //var servicename = "JKLService";

            var abcServer = new AbcServer();
            var result = abcServer.StopService(machine, user, servicename);

            Assert.IsTrue(result);
        }

        //Updated By Shariq -- Local PC Test
        [TestMethod]
        public void StartService052()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();
            var servicename = "ABCService";
            //var servicename = "DEFService";
            //var servicename = "JKLService";

            var abcServer = new AbcServer();
            var result = abcServer.StartService(machine, user, servicename);

            Assert.IsTrue(result);
        }

        //Updated By Shariq -- Local PC Test
        [TestMethod]
        public void RestartService052()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();
             var servicename = "ABCService";
            //var servicename = "DEFService";
            //var servicename = "JKLService";

            var administrationServer = new AdministrationServer();
            var result = abcServer.RestartService(machine, user, servicename);

            Assert.IsTrue(result);
        }

        //Updated By Shariq -- Local PC Test
        [TestMethod]
        public void ListServicesStatus052()
        {
            var machine = ABC.CommonFunctions.GetMachineName();
            var user = ABC.CommonFunctions.GetUserName();

            var abcServer = new AbcServer();
            var results = abcServer.ListServicesStatus(machine, user);

            Assert.IsTrue(results.Rows.Count > 0);
        }
    }

    public partial class ABCDataModel()
    {
        [TestMethod]
        public void TowerDatabaseQuery05()
        {
            using (var towerContext = new TowerModelContainer())
            {
                var results = towerContext.RTICLIENTs.Where(x => x.RTICLIENT_ID > 0);
                Assert.IsTrue(results.Any());
            }
        }

        [TestMethod]
        public void CadAdminDatabaseQuery051()
        {
            using (var CadAdminContext = new CadAdminModelContainer())
            {
                var results = CadAdminContext.TOWERIMPORTCLIENTs.Where(x => x.TOWERIMPORTCLIENT_ID > 0);
                Assert.IsTrue(results.Any());
            }
        }

        [TestMethod]
        public void CadAdminDatabaseQuery052()
        {
            using (var CadAdminContext = new CadAdminModelContainer())
            {
                var results = CadAdminContext.TOWERIMPORTCLIENTs.Where(x => x.PRACTICE.Contains("GMY18"));
                Assert.IsTrue(results.Any());
            }
        }

        [TestMethod]
        public void CadAdminDatabaseQuery053()
        {
            using (var CadAdminContext = new CadAdminModelContainer())
            {
                var results = CadAdminContext.TOWERIMPORTs.Where(x => x.CLIENTKEY.Equals(1899) && x.ISNEW.Equals("F"));
                Assert.IsTrue(results.Any());
            }
        }

        [TestMethod]
        public void CadAdminDatabaseQuery054()
        {
            using (var CadAdminContext = new CadAdminModelContainer())
            {
                var results = CadAdminContext.HL7_FTP_CONFIG.Where(x => x.REMOTESERVER.Equals("rtiops2"));
                Assert.IsTrue(results.Any());
            }
        }

        [TestMethod]
        public void CadAdminDatabaseQuery055()
        {
            using (var CadAdminContext = new CadAdminModelContainer())
            {
                var results = CadAdminContext.HL7_FTP_CONFIG.Count();
                Assert.IsTrue(results > 0);
            }
        }
    }

    public partial class DocumentManagerServiceIntegrationTests
    {
        [TestMethod]
        public void SearchPostBdRemote05()
        {
            var docType = "CK";
            var docDet = "AD";
            using (
                var docManagerServiceClient = new DocumentManagerServiceClient(VerifyUtils.DocMgrRemoteUrl,
                    VerifyUtils.DocMgrRemoteEndpointName))
            {
                var embillzSearchData = docManagerServiceClient.SearchPostBd(Environment.MachineName,
                    Environment.UserName, docType: docType, docDet: docDet);
                VerifyUtils.VerifyDataTable(embillzSearchData);
            }
        }
    }
}
